[![language](https://img.shields.io/badge/core%20language-bash--go-1313c3.svg?style=for-the-badge)]()
[![license](https://img.shields.io/badge/License-APACHE%202%2E0-orange.svg?style=for-the-badge)]()
[![release](https://img.shields.io/badge/release%20quality-pre--alpha-black.svg?style=for-the-badge)]()

| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | [![pipeline status](https://gitlab.com/ZORALab/devora/badges/master/pipeline.svg)]() | [![coverage report](https://gitlab.com/ZORALab/devora/badges/master/coverage.svg)]() |
| `next`   | [![pipeline status](https://gitlab.com/ZORALab/devora/badges/next/pipeline.svg)]() | [![coverage report](https://gitlab.com/ZORALab/devora/badges/next/coverage.svg)]() |

[![Devora-Banner](https://lh3.googleusercontent.com/vgsNcFf2qAdOG1lhU729-Lq2jNx2XXs-3JMPB0_cK5F2DikcZQob8k0ekra51DBoV3ntE96jqU2LJxgDOqlFDbGt9mR3lALDoo5dAj-IOyrz0lLsp5-hqdGXFGvPxSTdAEfijz285kmNnhLvX7HFMNmXqldZDa4cdn3xoN6P0PeT7HH_yDzwHaxpEl31L5y_kb4EC4anstgLusQrgtQWBKTXGzeU70ro3fovyM203XEXXPozsbHVHC4SNSqdnBqWFSWO5zP1JJHEWEsGaGZ3Pw0ycm1OySWOKGaFiblhidw-7BcTw_vVhW_EyEd8DjfGdFA38S7xAFjYmwhtJOKeitzTsenBmDWugg8UUXdLc6B3pRHOvynpAx0A6I78jFW9vi165xVNINfGxWDsWi4VO4BVKQaBKAu0kwy6F3sTXVbWP18sKZ6yHs14EMJBz5pBtRsZLEloIDwsARMoDM1qAeLanccKjTFD6ejHfDNcTKOhZE-PrPpMBKooBBJlSHI-6fCoMnDhU7msjUZbLQjT9pg0SutkRw-kObRWt_syjuepwNfnmrFGE4mjdSS-ZAvm1xOnHh3znRK8rFAXZ8Cu0Pt-ulVWlM60EmuAu45u84jXoFomtTbqynFtxeRRNsgcQAsqhTpekfcY79cZL48WE4KPcFzo7KziHl941Kzz0KXpS5Rdl7HHka7OuefqLN-yy5Edmnuze0p_OYOfIQ=w958-h479-no)]()


# Project Devora
A project altering the luks decryption on boot from using passphrase into using
usb stick. This frees up the `/boot` partition for full disk encryption and
keeping the disk decryption process simpiler (save human memory from 1 high
entropy passphrase) in exchange of a usb token.

<br/>

*This project is sponsored by:*

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)


<br/>

## Installing
to be filled.

<br/>

## Usage
to be filled.

<br/>

## Issues? Bugs? New Idea? Smelly Code?
Please feel free to raise a topic in our
[ISSUES](https://gitlab.com/ZORALab/devora/issues) section.

> If you want to discuss something, label it as ~"Discussion" label.
>
> If you recognize a bug and wants to report it, label it as ~"Bug" label.
>
> If you have a new idea or suggestion, label it as ~"Suggestion" label.
>
> If you still unsure what to label, label it as ~"Suggestion" label.


<br/>

## License
This repository is licensed specified in this file: [LICENSE](https://gitlab.com/ZORALab/devora/blob/master/LICENSE).

<br/>

## Contribute
### Feedbacks, New Merge Requests, etc.
Before contributing, please ensure you're fully abide to the following guidelines:
1. [Contributing 101](https://gitlab.com/ZORALab/devora/blob/master/CONTRIBUTING.md)


### Sponsor Us!
At ZORALab, we're committed to build more useful and practical open-source
software for free. However, To accelerate our efforts, this things doesn't come
cheap.

But you can help us by sponsoring us a one time small tip (can be a cup of
coffee) through our payment channel below! By sponsoring us, you'll get:
> 1. A customized `300px x 100px` badge designed and placed in this project
`sponsored` section!
> 2. Mentioned in the development and CHANGELOG!

Also, be sure to read the
[Terms and Conditions](https://gitlab.com/ZORALab/RESOURCES/blob/next/SPONSORSHIP.md).

> We know most people skips through reading the `Terms and Conditions` and
> went over our heads; Please hear us out by reading through it. We made it
> all our best making it easy for you to get throgh.

<br/>
To start, visit our payment gateway below and be sure to mention:
> 1. **MOST IMPORTANT** - Your `email` or `phone` contact. We'll communicate for
> designing your badge.
> 2. The `project` you're sponsoring.

[![Paypal](https://lh3.googleusercontent.com/8YAQpan47r2HKZn0BgdWQ2mfo0vlhrnQ0cKXbHrlt-R3PE3xeu4gO5KSVWWuFNILuMr9A5l9exMgNZ0EopBLXUocJh9F9fLZ4Q0USYR4QPax7RdAOnVl_K1fCIegPjB1thhfw2F43gd-d0gVPHURfKP1FZWKE_WMYLyyJV6UwCyE98CpjVqSzhRgCTCKTbilXfjJsrVVTLTCy8U9MA-m7NAYa67rvjZRkhVTPK7ncKZ1j4avNQxP4HdMR-F6sY3J-DQPsjNF3lXDxBxxLEdw0HrQQ2PLc4DhlHCa62N6dCs1zYNhoZPPGi8A36rPbynILwa1BK9CNeohlc1i5jVyIB6Fb1n-FiM_9L3yT3TYLcnQVH7M1LAqwsmDVgasE_wpG7-qDd5kVBaKAojkRcYbin93l2cl05M3VY0MKVkbE446D5cb_wJ-LJj6qL0az-Ut-u4cvsuT4Nnfl9E2GMt3mG2922B5kXJ-PadBIHB1YyGnhS8CAZJC5Te8TKZrdIld-tr2vOVhR0z2JB86dQUJ4vtsHeZAnazf_ad19gob-E5GJQPg2VDgwRfyvgMXDnMOKA-AJO4Tskohg9MPso4UN_vVsM3UwvVIc9mK1cB8BnrMBfm6Us-B6gT1XV4Teix1PgenhCNpZfFIK4vDtZKIuD8kSHE235w7=w200-h67-no)](https://www.paypal.me/zoralab/15)

**Thank you for helping us!**
